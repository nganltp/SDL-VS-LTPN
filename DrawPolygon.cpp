#include "DrawPolygon.h"
#include <iostream>

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float theta = M_PI / 2;

	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R * cos(theta) + 0.5);
		y[i] = yc - int(R * sin(theta) + 0.5);
		theta += 2 * M_PI / 3;
	}

	for (int i = 0; i < 3; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);
	}

}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float theta = 0;

	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R* cos(theta) + 0.5);
		y[i] = yc - int(R* sin(theta) + 0.5);
		theta += M_PI / 2;
	}

	for (int i = 0; i < 4; i++) {
		DDA_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float theta = M_PI / 2;

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R* cos(theta) + 0.5);
		y[i] = yc - int(R *sin(theta) + 0.5);
		theta += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float theta = M_PI / 2;

	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R* cos(theta) + 0.5);
		y[i] = yc - int(R* sin(theta) + 0.5);
		theta += M_PI / 3;
	}

	for (int i = 0; i < 6; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float theta = M_PI / 2;

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R* cos(theta) + 0.5);
		y[i] = yc - int(R *sin(theta) + 0.5);
		theta += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5], xn[5], yn[5];
	float theta = M_PI / 2;

	float R_nho = R * sin(M_PI / 10) / sin(7 * M_PI / 10);

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R* cos(theta) + 0.5);
		y[i] = yc - int(R* sin(theta) + 0.5);
		xn[i] = xc + int(R_nho * cos(theta + M_PI / 5) + 0.5);
		yn[i] = yc - int(R_nho * sin(theta + M_PI / 5) + 0.5);
		theta += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xn[i], yn[i], ren);
		DDA_Line(xn[i], yn[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8];
	float theta = M_PI / 2;

	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R* cos(theta) + 0.5);
		y[i] = yc - int(R *sin(theta) + 0.5);
		theta += 2 * M_PI / 8;
	}

	for (int i = 0; i < 8; i++)
	{
		DDA_Line(x[i], y[i], x[(i + 2) % 8], y[(i + 2) % 8], ren);
	}
}


//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5], xn[5], yn[5];
	float theta = startAngle;

	float R_nho = R * sin(M_PI / 10) / sin(7 * M_PI / 10);

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R* cos(theta) + 0.5);
		y[i] = yc - int(R* sin(theta) + 0.5);
		xn[i] = xc + int(R_nho * cos(theta + M_PI / 5) + 0.5);
		yn[i] = yc - int(R_nho * sin(theta + M_PI / 5) + 0.5);
		phi += 2 * M_PI / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		DDA_Line(x[i], y[i], xn[i], yn[i], ren);
		DDA_Line(xn[i], yn[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}


void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	if (r <= 0)
		return;

	int nStartPhi = 1;
	float StartPhi = nStartPhi * M_PI / 2;
	nStartPhi++;

	DrawStarAngle(xc, yc, r, StartPhi, ren);
	float R_nho = r * sin(M_PI / 10) / sin(7 * M_PI / 10);
	DrawConvergentStar(xc, yc, R_nho, ren);
	
}