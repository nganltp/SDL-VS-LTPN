#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	long a2 = a * a,
		b2 = b * b, 
		x = 0, y = b, 
		b2_ = 2 * b2, 
		a2_ = 2 * a2,
		p=0;
	Draw4Points(xc, yc, x, y, ren);
	p = (int)(b2 - (a2*b) + (a2/4));
	while (b2_*x < a2_*y)
	{
		x++;
		if (p < 0)
			p = p + (int)b2_*x+b2;
		else
		{
			y--;
			p =p + (int)b2_*x + b2 - a2_ * y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	p = (int)(b2*(x + 0.5)*(x + 0.5) + a2 * (y - 1)*(y - 1) - a2 * b2);
	while (y > 0)
	{
		y--;
		if (p > 0)
			p += a2 - a2_ * y;
		else
		{
			x++;
			p += a2 + b2_ * x - a2_ * y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int  x = 0, y = b, p;
	p = b*b - a*a*b + a*a / 4;
	while (2.0*b*b*x <= 2.0*a*a*y)
	{
		if (p < 0)
		{
			p = p + 2*b*b*x + b*b;
			x++;
		}
		else
		{
			p = p + 2*b*b*x - 2 * a*a*y - b * b;
			x++; 
			y--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
	p = b*b*(x + 0.5)*(x + 0.5) + a*a*(y - 1)*(y - 1) - a*a*b*b;
	while (y > 0)
	{
		if (p <= 0)
		{
			p = p + 2 * b*b*x - 2 * a*a*y + a* a;
			x++; 
			y--;
		}
		else
		{
			p = p - 2*a*a*y + a*a;
			y--;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}